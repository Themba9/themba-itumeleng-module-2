void main() {
  List<String> winningApps = [
    "FNB (2012)",
    "SnapScan (2013)",
    "LIVE Inspect (2014)",
    "WumDrop (2015)",
    "Domestly (2016)",
    "Shyft (2017)",
    "Khula (2018)",
    "Naked Insurance (2019)",
    "EasyEquities (2020)",
    "Ambani Africa (2021)"
  ];

  //Sorting and printing apps by names:
  winningApps.sort();
  print(winningApps);

  //Printing the 2017 and 2018 winning apps:
  print("\nThe 2017 Winning App: ${winningApps.elementAt(7)}");
  print("The 2018 Winning App: ${winningApps.elementAt(4)}");

  //The total number of apps in the array:
  print("\nTotal Number of Winnings Apps: ${winningApps.length}");
}
