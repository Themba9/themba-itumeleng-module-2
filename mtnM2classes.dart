class App {
  String appName = "";
  String appSector = "";
  String appDeveloper = "";
  int yearWon = 0;

  //function to print out the app name in capital letters
  void printName() {
    print(appName.toUpperCase());
  }
}

void main() {
  App FNB = App(); //Object
  FNB.appName = "FNB Banking App";
  FNB.appSector = "Banking Sector";
  FNB.appDeveloper = "FNB";
  FNB.yearWon = 2012;

  print("App Name: ${FNB.appName}");
  print("App Sector: ${FNB.appSector}");
  print("App Developer: ${FNB.appDeveloper}");
  print("Winning Year: ${FNB.yearWon}\n");

  FNB.printName(); //Calling the function to transform the app name to all capital letters
}
